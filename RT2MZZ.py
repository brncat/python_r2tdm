#!/usr/bin/env python
import numpy as np
from numpy import linalg as LA
import h5py
import sys
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# --------------------------------------------------------------------------------------
# Created by: BRUNO TENORIO in Mordor, year 3019 of The Third Age
# It reads the file auger_dyson.dat containing the CMO1,CMO2 and the 2-e rTDM.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# READ some information from Density files provided by OpenMolcas:
f=open('auger_dyson.dat',"r")
lines=f.readlines()
# 1st line, Total symmetry of WF product <N-1,N>:
totalSymmetry=int(lines[0])
# 2nd line, Number of irreps (symmetry number):
symmetry=int(lines[1])
# 3rd line, Number of basis functions (per irrep):
nbasf= [int(num) for num in lines[2].split()]
# 4th line, Number of active MOs (per irrep):
nash= [int(num) for num in lines[3].split()]
# 5th line, Number of total MOs (per irrep). NActive + NInactive:
nmo= [int(num) for num in lines[4].split()]
f.close()


# "The Molcas input file with the Densities, for example, r2TM_HS_005_003 must be given
# without any comment lines statrting with '#' in a file called auger_dyson.dat.
# It can be done by using the bash command in one line:"
#$ cat r2TM_HS_005_003 | grep -v "#" >& auger_dyson.dat

# The computation of one-electron conjugated Dyson orbitals
conjdys= False
#
# print one-particle Dyson orbital in AO basis
print_direct_dys=False
#
# Perform RI integrals for Auger intensities
#auger_ri_true=True
auger_ri_true=False
#
####################################################################################################
# Compute approximated intensities via projection into single seta atomic basis. Aka Fink's approach
fink_projection=False
#
if fink_projection:
# -----------------------------------------------
# -----------------------------------------------
#         Definition of the pure atomic orbitas
#         in the contracted basis.    Still to be improved....
    # CO C2v set ...
    #shell=[[1,2,4,8,9,11], [15,18], [], [23,26]] # means 1s,2s,2pz,2px,2py in the symmetry adapted basis index. cc-pvdz 
    #shell=[[1,2,5,14,15,18], [27,34], [], [47,54]] # for cc-pVTZ c2v 
    # Only shell_py is really used. shell is used for printing only.
    #shell_py=[[1,2,4,8,9,11], [1,4], [], [1,4]] cc-pvdz 
    #shell_py=[[1,2,5,14,15,18], [1,8], [], [1,8]] # for cc-pVTZ C2v  
    # CO no symmetry set
    #shell=[[1,2,11,31,32,41,5,35,8,38]] # means C1s,C2s,C2pz, O1s,O2s,O2pz, C2px,O2px, C2py,O2py cc-pvtz no symmetry
    #shell_py=[[1,2,11,31,32,41,5,35,8,38]] # cc-pvtz no symmetry

    # H2O C2v set ... [[O1s,O2s,O2p,H1s],[O2px],[],[O2py,H1s]]
    #shell=[[1,2,6,20], [30], [], [54,65]]
    #shell_py=[[1,2,6,20], [1], [], [1,12]] # aug-cc-pvtz

    # H2O C1 set ... [[O1s,O2s,O2px,O2py,H1s,H1s]]
    #shell=[[1,2,6,10,14,47,64]]
    #shell_py=[[1,2,6,10,14,47,64]] # aug-cc-pvtz    

    #HNCO Cs...
    #shell= [[1,2,4,6,11,12,15,18,31,32,34,36,41],[45,49,59]]
    #shell_py= [[1,2,4,6,11,12,15,18,31,32,34,36,41],[1,5,15]]

    # CO2 C2v .
    #shell=[[1,2,5,14,15,18,27,28,31],[40,47,54],[],[70,77,84]]
    #shell_py=[[1,2,5,14,15,18,27,28,31],[1,8,15],[],[1,8,15]]
   
    # NO2 C2v ..
    #shell=[[1,2,5,14,15,18,21], [34,41], [54], [64,71,72,75,78]]
    #shell_py=[[1,2,5,14,15,18,21], [1,8], [4], [1,8,9,12,15]]

    # N2 C2v cc-pvtz [[N1 1s, N1 2s, N1 2pz, N2 1s, N2 2s, N2 2pz],[N1 2px, N2 2px],[],[N1 2py, N2 2py]]
    #shell= [[1,2,5,14,15,18], [27,34], [], [47,54]]
    #shell_py= [[1,2,5,14,15,18], [1,8], [], [1,8]]

    # NE d-aug-cc-pVQZ. For Ne use NE with capital letters in OCA_center='NE 1s'
    #shell=[[1,2],[],[],[],[],[37],[51],[65]]
    #shell_py=[[1,2],[],[],[],[],[1],[1],[1]]

    # Cl aug-cc-pVQZ. For Cl use CL with capital letters in OCA_center='CL 1s'
    # 1s,2s,3s,2py,3py,2pz,3pz,2px,3px
    shell=[[1,2,3],[],[],[],[],[36,37],[48,49],[60,61]]
    shell_py=[[1,2,3],[],[],[],[],[1,2],[1,2],[1,2]]

    # Pyrimidine Cs...
    #shell=[[1,2,5,8,21,22,25,28,41,42,44,46,51,52,54,56,61,62,64,66,71,72,74,76,81,85,89,93],[97,107,117,121,125,129]]
    #shell_py=[[1,2,5,8,21,22,25,28,41,42,44,46,51,52,54,56,61,62,64,66,71,72,74,76,81,85,89,93],[1,11,21,25,29,33]] 
    
    # Pyrimidine C1... OCA_center='N1 1s'
    #shell=[[1,2,5,8,11,31,32,35,38,41,61,62,64,66,68,75,76,78,80,82,89,90,92,94,96,103,104,106,108,110,117,122,127,132]]
    #shell_py=[[1,2,5,8,11,31,32,35,38,41,61,62,64,66,68,75,76,78,80,82,89,90,92,94,96,103,104,106,108,110,117,122,127,132]]
 
#######################################################################

    # for equivalent atoms, like in the N2 molecule, where I have N1 and N2: OCA_center='N1 1s' to project on the N1 atom.
    #
    OCA_center='CL 1s'
#
#
    OCA_atom=OCA_center[:2] # takes the element from OCA_center
    n1=OCA_atom.replace(" ","") # these lines eliminate spaces and digits of equivalent atoms (if present)
    n1=n1.replace("1","")    
    n1=n1.replace("2","")
    n1=n1.replace("3","")
    n1=n1.replace("4","")
    n1=n1.replace("5","")
    n1=n1.replace("6","")
    n1=n1.replace("7","")
    n1=n1.replace("8","")
    n1=n1.replace("9","")
    OCA_atom=n1

if fink_projection and conjdys:
    print('One does not simply get Conjugate and Fink projection at the same time.')
    sys.exit()

####################################################################################################
####################################################################################################

#define number of orbitals, nmo, and number basis func per symmetry, nbasis, from input.
cmotab=list(nmo[ind]*nbasf[ind] for ind in range(symmetry))
tdmtab=list(nmo[ind]**3 for ind in range(symmetry))

#ncmo=total number of MO components
ncmo=int(sum(cmotab))
#nbasft = total number of basis function
nbasft=int(sum(nbasf))
#nasht=number of active orbitals
nasht=int(sum(nash))
#nosht=total number of orbitals
nosht=int(sum(nmo))
#comtaboff: number of orb in previus symm
comtaboff=list()
comtbasoff=list()
comtbasoff2=list()
comtcmoff=list()
comtnashoff=list()
off=0
for x in cmotab:
    comtaboff.append(off)
    off=off+x
off=0
for y in nbasf:
    comtbasoff.append(off)
    off=off+y
off=0
for v in nbasf:
    comtbasoff2.append(off)
    off=off+v**2
off=0 
for z in nmo:  # number of previus occupied orbitals
    comtcmoff.append(off)
    off=off+z
off=0
for z in nash:  # number of previus active orbitals
    comtnashoff.append(off)
    off=off+z

# identify the symmetry of the dipole operator
def symop(n):
# n=1,2,3->x,y,z
    if symmetry==4:
# (a1,b1,a2,b2)->(1,2,3,4)
# b1->x, b2->y, a1->z
        if n==1:
            return 2
        if n==2:
            return 4
        if n==3:
            return 1
    elif symmetry==8:
# (Ag,B2g,B1g,B3g,Au,B2u,B1u,B3u)->(1,2,3,4,5,6,7,8)
# B3u->x, B2u->y, B1u->z
        if n==1:
            return 8
        if n==2:
            return 6
        if n==3:
            return 7
    elif symmetry==2:
# A'->x,y, A"->z
        if n==1:
            return 1
        if n==2:
            return 1
        if n==3:
            return 2
    elif symmetry==1:
        return 1
    else:
        return 'incorrect symmetry group'

def symoprep(n):
# gigen the symmetry number of the operator
# returns the component as x,y z
    if symmetry==4:
        if n==1:
            return 'z'
        if n==2:
            return 'x'
        if n==3:
            return 'no dipole'
        if n==4:
            return 'y'
    elif symmetry==8:
        if n==6:
            return 'y'
        if n==7:
            return 'z'
        if n==8:
            return 'x'
        else:
            return 'no dipole'
    if symmetry==2:
        if n==1:
            return 'x'
        if n==2:
            return 'z'
    elif symmetry==1:
        return 1
    else:
        return 'incorrect symmetry group'

# multiplication table. for now only C2v, D2h, Cs and c1
def mul(i,j):
# (a1,b1,a2,b2)->(1,2,3,4)
    if symmetry==4:
        if i and j <= 4:
            if i ==1:
                return i*j
            if i==2:
                if j==1:
                    return 2
                if j==2:
                    return 1
                if j==3:
                    return 4
                if j==4:
                    return 3
            if i ==3:
                if j==1:
                    return 3
                if j==2:
                    return 4
                if j==3:
                    return 1
                if j==4:
                    return 2
            if i==4:
                if j==1:
                    return 4
                if j==2:
                    return 3
                if j==3:
                    return 2
                if j==4:
                    return 1
# (A',A")->(1,2)
    if symmetry==2:
        if i and j <= 2:
            if i ==1:
                return i*j
            if i==2:
                if j==1:
                    return 2
                if j==2:
                    return 1
    elif symmetry==8:
# (Ag,B2g,B1g,B3g,Au,B2u,B1u,B3u)->(1,2,3,4,5,6,7,8)
        if i and j <= 8:
            if i ==1:
                return i*j
            if i==2:
                if j==1:
                    return 2
                if j==2:
                    return 1
                if j==3:
                    return 4
                if j==4:
                    return 3
                if j==5:
                    return 6
                if j==6:
                    return 5
                if j==7:
                    return 8
                if j==8:
                    return 7
            if i ==3:
                if j==1:
                    return 3
                if j==2:
                    return 4
                if j==3:
                    return 1
                if j==4:
                    return 2
                if j==5:
                    return 7
                if j==6:
                    return 8
                if j==7:
                    return 5
                if j==8:
                    return 6
            if i==4:
                if j==1:
                    return 4
                if j==2:
                    return 3
                if j==3:
                    return 2
                if j==4:
                    return 1
                if j==5:
                    return 8
                if j==6:
                    return 7
                if j==7:
                    return 6
                if j==8:
                    return 5
            if i==5:
                if j==1:
                    return 5
                if j==2:
                    return 6
                if j==3:
                    return 7
                if j==4:
                    return 8
                if j==5:
                    return 1
                if j==6:
                    return 2
                if j==7:
                    return 3
                if j==8:
                    return 4
            if i==6:
                if j==1:
                    return 6
                if j==2:
                    return 5
                if j==3:
                    return 8
                if j==4:
                    return 7
                if j==5:
                    return 2
                if j==6:
                    return 1
                if j==7:
                    return 4
                if j==8:
                    return 3
            if i==7:
                if j==1:
                    return 7
                if j==2:
                    return 8
                if j==3:
                    return 5
                if j==4:
                    return 6
                if j==5:
                    return 3
                if j==6:
                    return 4
                if j==7:
                    return 1
                if j==8:
                    return 2
            if i==8:
                if j==1:
                    return 8
                if j==2:
                    return 7
                if j==3:
                    return 6
                if j==4:
                    return 5
                if j==5:
                    return 4
                if j==6:
                    return 3
                if j==7:
                    return 2
                if j==8:
                    return 1
    elif symmetry==1:
        return 1
    else:
        return 'incorrect symmetry group'

#norb catch the number of orbitals in the i symmetry
def norb(i):
    #returns the number of orbitals in the i symm
    return int(nmo[i-1])

#nbasis catch the number of basis functions in the i symmetry
def nbasis(i):
    #returns the number of basis function in the i symm
    return int(nbasf[i-1])

#catch cmo takes the corresponding symmetry CMO for multiplication
def catchcmo(i):
    #x=Num orb in symm
    x=int(nmo[i-1])
    #y=Num basis in symm
    y=int(nbasf[i-1])
#returns a product #orb*#basis and a tuple (#Orb,#Basis)
    return (x*y,(y,x))

### IF DOING ONE CENTER APPROXIMATION
if fink_projection:
    def nbasis_sz(i):
        #returns the number of basis function in the i symm
        return int(sz_nbasf[i-1])

    def catchcmo_sz(i):
        #x=Num orb in symm
        x=int(nmo[i-1])
        #y=Num basis in symm
        y=int(sz_nbasf[i-1])
        #returns a product #orb*#basis and a tuple (#Orb,#Basis)
        return (x*y,(y,x))

#___________________________________________________________
# basis_func_id is a function to read a list containing (c,n,l,m)
# with c the atomic center, n the principal quantum number,
# l the angular quantum number, and m the projection of l 
# usefull convention is that l=0,1,-1 is z,x,y respectively.
# d0,d+1,d-1,d+2,d-2 -> z2,xz,yz,xy,x2-y2
#
# Works only with full spherical basis


h = h5py.File('scf.h5', 'r')
basis_id_hd5=np.reshape( h['BASIS_FUNCTION_IDS'],(nbasft,4))
element=h['CENTER_LABELS']
n_elements=len(element)
element=np.reshape(element,(n_elements))
element=np.array(element, dtype =np.str)
element=np.char.strip(element)
# desymmtrized element list
if symmetry==1:
# in case I dont use symmetry there is no 
# symmetry equivalent elements
	element_desym=element
	n_elements_desym=n_elements
else:
# in case I use symmetry,
# I may have different symmetry elements
	element_desym=h['DESYM_CENTER_LABELS']
	n_elements_desym=len(element_desym)
	element_desym=np.reshape(element_desym,(n_elements_desym))
	element_desym=np.array(element_desym, dtype =np.str)
	element_desym=np.char.strip(element_desym)
h.close()
#print(basis_id_hd5)

def basis_func_id(cnlm):
    c=cnlm[0]
    n=cnlm[1]
    l=cnlm[2]
    m=cnlm[3]
    list_element=element
#
    if l==0:
        func_id='s'
    if l==1:
        if m==0:
            func_id='pz'
        if m==1:
            func_id='px'
        if m==-1:
            func_id='py'
    if l==2:
        if m==0:
            func_id='d0'
        if m==1:
            func_id='d+1'
        if m==-1:
            func_id='d-1'
        if m==2:
            func_id='d+2'
        if m==-2:
            func_id='d-2'
    if l==3:
        if m==0:
            func_id='f0'
        if m==1:
            func_id='f+1'
        if m==-1:
            func_id='f-1'
        if m==2:
            func_id='f+2'
        if m==-2:
            func_id='f-2'
        if m==3:
            func_id='f+3'
        if m==-3:
            func_id='f-3'
    return '{}{}{}{}'.format(list_element[c-1]," ",n+l, func_id)
# The following order of D, F en G functions is expected:
# 5D: D 0, D+1, D-1, D+2, D-2
# 7F: F 0, F+1, F-1, F+2, F-2, F+3, F-3
# 9G: G 0, G+1, G-1, G+2, G-2, G+3, G-3, G+4, G-4

# now I make a list len(nbasft) with all basis ids
basis_id_list=list()
for i in range(nbasft):
    basis_id_list.append(basis_func_id(basis_id_hd5[i]))
#print('basis id')

#___________________________________________________________
def phase_lm(cnlm):
    phase_coeff=1.0
    # phase_lm reads a list of integers (c,n,l,m) so to multiply
    # func.coeff by -1 if the centers basis change phase with the irrep. transformation.
    # See the "Symmetry adapted Basis Functions" on your molcas.log file.
    if symmetry==4: #C2v
        # py
        if (cnlm[2] ==1 and cnlm[3] ==-1):
            phase_coeff=-1.0
        # d functions: d-1, d-2
        if (cnlm[2] ==2 and cnlm[3] ==-1):
            phase_coeff=-1.0
        if (cnlm[2] ==2 and cnlm[3] ==-2):
            phase_coeff=-1.0
        # f functions: f-1, f-2, f-3
        if (cnlm[2] ==3 and cnlm[3] ==-1):
            phase_coeff=-1.0
        if (cnlm[2] ==3 and cnlm[3] ==-2):
            phase_coeff=-1.0
        if (cnlm[2] ==3 and cnlm[3] ==-3):
            phase_coeff=-1.0 
    return phase_coeff

#___________________________________________________________
" tomolden(orbital) is a program to receive an orbital as symmetrized basis and print as Molden format"
# the orbital input should be a flat list.
# for now I am accepting up to 12 different symmetric equivalalent elements
def tomolden(orb):
# search for equal list elements and equalize
	search=basis_id_hd5[:,:4].astype(int)
	orbital=np.array(orb)
	orbital2=np.array(orb)
	#loop over search
	for s in range(len(search)):
		for h in range(len(search)-s-1):
			if all(search[s]==search[h+s+1]):
				#this will be true if I have symmetricaly equivalent atoms
				# use function phase_lm to decide whether orb* + or - 1.
				phase_coeff=phase_lm(search[s])
				orbital[s]=(orbital2[s]+orbital2[h+s+1])/np.sqrt(2)
				orbital[h+s+1]=(orbital2[s]-orbital2[h+s+1])*phase_coeff/np.sqrt(2)
				#here i am saying there is another atom of the same element
				search[h+s+1][0]=int(search[h+s+1][0]+400)
	#
	lista = np.ndarray((nbasft,5),dtype = object)
	lista[:,:4]=search[:,:4].astype(int)
	lista[:,4]=orbital[:].astype(float)
	#
	tempfile1=[]
	tempfile2=[]
	tempfile3=[]
	tempfile4=[]
	tempfile5=[]
	tempfile6=[]
	tempfile7=[]
	tempfile8=[]
	tempfile9=[]
	tempfile10=[]
	tempfile11=[]
	tempfile12=[]
	tempfile13=[]
	tempfile14=[]
	tempfile15=[]
	tempfile16=[]
	tempfile17=[]
	tempfile18=[]
	tempfile19=[]
	tempfile20=[]
	tempfile21=[]
	tempfile22=[]
	tempfile23=[]
	tempfile24=[]
	tempfile99=[]
	molden=[]
	molden1=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	max_ele=n_elements
	# separate the list by different elements
	for j in range(len(lista)):
		if lista[j][0] == 1:
			tempfile1.append(lista[j].tolist()) 
		if lista[j][0] == 401:
			tempfile2.append(lista[j].tolist())
		if lista[j][0] == 2:
			tempfile3.append(lista[j].tolist())
		if lista[j][0] == 402:
			tempfile4.append(lista[j].tolist())
		if lista[j][0] == 3:
			tempfile5.append(lista[j].tolist())
		if lista[j][0] == 403:
			tempfile6.append(lista[j].tolist())
		if lista[j][0] == 4:
			tempfile7.append(lista[j].tolist())
		if lista[j][0] == 404:
			tempfile8.append(lista[j].tolist())
		if lista[j][0] == 5:
			tempfile9.append(lista[j].tolist())
		if lista[j][0] == 405:
			tempfile10.append(lista[j].tolist())
		if lista[j][0] == 6:
			tempfile11.append(lista[j].tolist())
		if lista[j][0] == 406:
			tempfile12.append(lista[j].tolist())
		if lista[j][0] == 7:
			tempfile13.append(lista[j].tolist())
		if lista[j][0] == 407:
			tempfile14.append(lista[j].tolist())
		if lista[j][0] == 8:
			tempfile15.append(lista[j].tolist())
		if lista[j][0] == 408:
			tempfile16.append(lista[j].tolist())
		if lista[j][0] == 9:
                        tempfile17.append(lista[j].tolist())
		if lista[j][0] == 409:
			tempfile18.append(lista[j].tolist())
		if lista[j][0] == 10:
                        tempfile19.append(lista[j].tolist())
		if lista[j][0] == 410:
			tempfile20.append(lista[j].tolist())
		if lista[j][0] == 11:
                        tempfile21.append(lista[j].tolist())
		if lista[j][0] == 411:
			tempfile22.append(lista[j].tolist())
		if lista[j][0] == 12:
                        tempfile23.append(lista[j].tolist())
		if lista[j][0] == 412:
			tempfile24.append(lista[j].tolist())
	tempfile99.append(tempfile1)
	tempfile99.append(tempfile2)
	tempfile99.append(tempfile3)
	tempfile99.append(tempfile4)
	tempfile99.append(tempfile5)
	tempfile99.append(tempfile6)
	tempfile99.append(tempfile7)
	tempfile99.append(tempfile8)
	tempfile99.append(tempfile9)
	tempfile99.append(tempfile10)
	tempfile99.append(tempfile11)
	tempfile99.append(tempfile12)
	tempfile99.append(tempfile13)
	tempfile99.append(tempfile14)
	tempfile99.append(tempfile15)
	tempfile99.append(tempfile16)
	tempfile99.append(tempfile17)
	tempfile99.append(tempfile18)
	tempfile99.append(tempfile19)
	tempfile99.append(tempfile20)
	tempfile99.append(tempfile21)
	tempfile99.append(tempfile22)
	tempfile99.append(tempfile23)
	tempfile99.append(tempfile24)
	list99=tempfile99
	# the next line is a single line command to remove empty lists of a list.
	tempfile99 = [x for x in list99 if x != []]
	for e in range(n_elements_desym):
		xt=tempfile99[e]
		xt=np.array(xt)
		s=[]
		px=[]
		py=[]
		pz=[]
		d0=[]
		dp1=[]
		dm1=[]
		dp2=[]
		dm2=[]
		f0=[]
		fp1=[]
		fm1=[]
		fp2=[]
		fm2=[]
		fp3=[]
		fm3=[]
		p=[]
		d=[]
		f=[]
		xm=list()
		max_ang=max(xt[:,2])
		for j in range(len(xt)):
			if xt[j][2] == 0:
				s.append(xt[j])
			if xt[j][2] == 1:
				if xt[j][3] == 0:
					pz.append(xt[j])
				if xt[j][3] == 1:
					px.append(xt[j])
				if xt[j][3] == -1:
					py.append(xt[j])
			if xt[j][2] == 2:
				if xt[j][3] == 0:
					d0.append(xt[j])
				if xt[j][3] == 1:
					dp1.append(xt[j])
				if xt[j][3] == -1:
					dm1.append(xt[j])
				if xt[j][3] == 2:
					dp2.append(xt[j])
				if xt[j][3] == -2:
					dm2.append(xt[j])
			if xt[j][2] == 3:
				if xt[j][3] == 0:
					f0.append(xt[j])
				if xt[j][3] == 1:
					fp1.append(xt[j])
				if xt[j][3] == -1:
					fm1.append(xt[j])
				if xt[j][3] == 2:
					fp2.append(xt[j])
				if xt[j][3] == -2:
					fm2.append(xt[j])
				if xt[j][3] == 3:
					fp3.append(xt[j])
				if xt[j][3] == -3:
					fm3.append(xt[j])
		if max_ang >= 0:
			s=np.array(s)
			s=s.tolist()
			xm.append(s)
		if max_ang >= 1:
			#p=px+py+pz
			#py=np.array(py)
			for m in range(len(py)):
				p.append(px[m])
				p.append(py[m])
				p.append(pz[m])
			#p=p[p[:, 1].argsort()]
			#p=p.tolist()
			xm.append(p)
		if max_ang >= 2:
			#d=d0+dp1+dm1+dp2+dm2
			#d=np.array(d)
			#d=d[d[:, 1].argsort()]
			#d=d.tolist()
			for m in range(len(d0)):
				d.append(d0[m])
				d.append(dp1[m])
				d.append(dm1[m])
				d.append(dp2[m])
				d.append(dm2[m])
			xm.append(d)
		if max_ang >= 3:
			#f=f0+fp1+fm1+fp2+fm2+fp3+fm3
			#f=np.array(f)
			#f=f[f[:, 1].argsort()]
			#f=f.tolist()
			for m in range(len(f0)):
				f.append(f0[m])
				f.append(fp1[m])
				f.append(fm1[m])
				f.append(fp2[m])
				f.append(fm2[m])
				f.append(fp3[m])
				f.append(fm3[m])
			xm.append(f)
		xm=np.concatenate(xm)
		molden1[e]=xm
	for w in range(n_elements_desym):
		molden1[w]=molden1[w].tolist()
		molden.append(molden1[w])
	molden=np.concatenate(molden)
	if max(molden[:,0])>400:
	# this if will be true if I did change the index for some symmetry equivalent atom
		for at in range(len(molden[:,0])):
	# here I changing the extra element index back as in the original basis_id_hd5
			if molden[at][0] > 400:
				molden[at][0]=int(molden[at][0]-400)
	#print('MOLDEN FILE Dyson')
	molden_print=molden[:,:4].astype(int)
	molden_ao=np.array(molden[:,4])
	
	basis_id_list=list()
	for i in range(nbasft):
		basis_id_list.append(basis_func_id(molden_print[i]))
	#print('# One particle direct Dyson orbital in MOLDEN format')
	for j in range(nbasft):
		print( '{:^3} {:7} {:19.16f}'.format(j+1,basis_id_list[j],molden_ao[j]) )
	return
#___________________________________________________________

# Collecting data from the file 

cmob=list()
cmoa=list()
tdmab=list()
dyson=list()
# Get here overlap matrix
#if conjdys or print_direct_dys or fink_projection:
if True:
    ao_dip=list()
    h = h5py.File('scf.h5', 'r')
    ao_dipx=np.reshape( h['AO_MLTPL_X'],(nbasft,nbasft))
    ao_dipy=np.reshape( h['AO_MLTPL_Y'],(nbasft,nbasft))
    ao_dipz=np.reshape( h['AO_MLTPL_Z'],(nbasft,nbasft))
    ao_dip=(ao_dipx,ao_dipy,ao_dipz)
    over_length=0
    for x in range(symmetry):
        over_length=over_length+(nbasis(x+1)**2)
    ao_overlap=np.reshape( h['AO_OVERLAP_MATRIX'],(over_length))
    h.close()
f=open('auger_dyson.dat',"r")
lines=f.readlines()
# 1st line, Total symmetry of WF product <N-1,N>:
# CMO1 comes from PSI1 that is the cation.
# CMO2 comes from PSI2 that is tne neutral
#totalSymmetry=int(lines[0])

# number previus_lines=5
for z in range(ncmo):
    cmoa.append(float(lines[z+5]))  #cmoa = cmo1
    cmob.append(float(lines[z+ncmo+5])) #cmob = cmo2

for x in range(nosht):
    val=float(lines[2*ncmo+x+5].split()[1])
    dyson.append(val)

ntdab=int(lines[2*ncmo+nosht+5])
for y in range(ntdab):
    val=float(lines[2*ncmo+nosht+y+5+1].split()[3])
    tdmab.append(val)
f.close()

#==================================
def mo2ao(mo_orb,cmo_ab):
    # function mo2ao transforms MO to AO using the transformation matrix cmo_ab.
    # store block symmetry for orbital
    total_orb=list(0 for i in range(symmetry))
    total_orb_ao=list(0 for i in range(symmetry))
    for s in range(symmetry):
        isym=s+1
        total_orb[s]=mo_orb[comtcmoff[s]:comtcmoff[s]+norb(isym)]
    # Computing orbital AO 
    for l in range(symmetry):
        lsym=l+1
        nol=norb(lsym)
        nbl=nbasis(lsym)
        # direct Dyson AO transformation
        q=catchcmo(lsym)
        cmon=cmo_ab[comtaboff[l]:comtaboff[l]+cmotab[l]]
        cmon=np.reshape(cmon,q[1],order='F')
        orb_mo=total_orb[l]
        orb_ao = np.einsum('ij,j->i', cmon, orb_mo)
#       store symmetry block in direct_dys_ao
        total_orb_ao[l]=orb_ao
    total_orb_ao2=[val for sublist in total_orb_ao for val in sublist] #Flatten the list
    return total_orb_ao2

#================================================
#================================================
# projection into atomic "single-zeta" basis set.
# So-called Fink approach

if fink_projection:
    # the list shell_py is the shell list in pythonic way...
    shell_basis=[(fg-1) for fg in [val for sublist in shell for val in sublist] ]
    atombasis_list=[basis_id_list[ei] for ei in shell_basis]

    # The file OCA.dat has 21 lines of integrals for each element. 
    # Available elements (currently) are: C, O, N, Ne
    # Reads 5 columns: I,J,L,M,G
    #  I,J: 1   2    3    4
    #       2s  2pz  2px  2py
    oca=list()
    nele_oca=21
    with open('OCA.dat',"r") as oc :
        for curline in oc:
            if curline.startswith("#"):
                pass
            else:
                line3=[float(elem) for elem in curline.split()]
                oca.append(line3)
    temp_oca = [x for x in oca if x != []] # 
    oca=np.array(temp_oca)
    #Carbon set
    oca_C=oca[:nele_oca]
    #Oxygen set
    oca_O=oca[nele_oca:nele_oca*2]
    #N set
    oca_N=oca[nele_oca*2:nele_oca*3]
    #Ne set
    oca_NE=oca[nele_oca*3:nele_oca*4]
    #Cl set
    oca_CL=oca[nele_oca*4:nele_oca*5]
   
    # Define One Center Integral according to the OCA_atom variable
    if OCA_atom=='C':
        OCI=oca_C
    elif OCA_atom=='O':
        OCI=oca_O
    elif OCA_atom=='N':
        OCI=oca_N
    elif OCA_atom=='NE':
        OCI=oca_NE
    elif OCA_atom=='CL':
        OCI=oca_CL
 
    def elmij(c,i,j,l,m):
        ver = 0.0
        if c ==OCA_center:
            for ez in OCI:
                if i == '2s':
                    ii=1
                elif i == '2pz':
                    ii=2
                elif i == '2px':
                    ii=3
                elif i == '2py':
                    ii=4
                else:
                    break
                if j == '2s':
                    jj=1
                elif j == '2pz':
                    jj=2
                elif j == '2px':
                    jj=3
                elif j == '2py':
                    jj=4
                else:
                    break

                if all([ii,jj,l,m] == ez[:4]) :
                    ver = ez[4]
        return ver
    #print('Eml',elmij('C 1s','2py','2px',2,-2)) # this test should give: Eml 0.006919730033

    #sz_nbasf = number of basis elements in SZ for each irrep
    sz_nbasf=[len(im) for im in shell_py ]
    #nbasft = total number of basis function
    sznbasft=int(sum(sz_nbasf))

    # ncsz = number of elements in the overlap matrix in SZ basis. 
    # nbasz = number of elements in matrix B_sz
    ncsz= list(sz_nbasf[ind]**2 for ind in range(symmetry))
    nbasz=list(sz_nbasf[ind]*nbasf[ind] for ind in range(symmetry))
    nszorb=list(sz_nbasf[ind]*nmo[ind] for ind in range(symmetry)) # similar to cmotab

    ncszoff=list()  # number of previus elements in S_sz
    off=0
    for ts in ncsz: 
        ncszoff.append(off)
        off=off+ts   
    nbaszoff=list() # number of previus elements in B_sz
    off=0
    for ts in nbasz: 
        nbaszoff.append(off)
        off=off+ts
    nszoff=list() # number of previus basis elements in D_sz
    off=0
    for ts in sz_nbasf:
        nszoff.append(off)
        off=off+ts
    norbsztaboff=list() # number of previus elements in D_sz. Similar to comtaboff
    off=0
    for ts in nszorb:
        norbsztaboff.append(off)
        off=off+ts
# ------------------------------------------

    sz_ovl=list()
    nbsfoff2=0
    for sim in range(symmetry):
        #print(shell_py[sim],nbsfoff2)
        for i in shell_py[sim]:
            for j in shell_py[sim]:
                if i>0 and j>0:
     #              print(i,j)
                    ij_sz=i + (j-1)*nbasf[sim] + nbsfoff2 -1
                    #print(ij_sz,ao_overlap[ij_sz])
                    sz_ovl.append(ao_overlap[ij_sz])
        nbsfoff2=nbsfoff2+nbasf[sim]**2
    
    sz_b=list()
    nbsfoff2=0
    for sim in range(symmetry):
        for k in shell_py[sim]:
            for l in range(nbasf[sim]+1):
                if k > 0 and l > 0 : #
                    kl_sz=k + (l-1)*nbasf[sim] + nbsfoff2 -1
                    sz_b.append(ao_overlap[kl_sz])
        nbsfoff2=nbsfoff2+nbasf[sim]**2
#
    #print('Length of B', len(sz_b))
#   Multiplication of inverse of S_sz with B_sz
    d_sza_sym=list()
    d_szb_sym=list()
    #d_A_sza_sym=list()
    #d_A_szb_sym=list()
    for sim in range(symmetry):
        # get Sz for irrep and invert
        sz_reshape=np.array(sz_ovl[ncszoff[sim]:ncszoff[sim]+ncsz[sim]]).reshape((sz_nbasf[sim],sz_nbasf[sim]))
        la_inv_sz=LA.inv(sz_reshape)
        #print('S')
        #with np.printoptions(precision=3, suppress=True):
        #    print(sz_reshape)
        #print('S.S-1')
        #with np.printoptions(precision=3, suppress=True):
        #    print(np.dot(la_inv_sz,sz_reshape))
        #get B
        b_ovl=np.array(sz_b[nbaszoff[sim]:nbaszoff[sim]+nbasz[sim]]).reshape((sz_nbasf[sim],nbasf[sim]))
        #print('B',np.shape(b_ovl))
        #with np.printoptions(precision=4, suppress=True):
        #    print(b_ovl)       
        tb_sz = np.einsum('ik,kj->ij', la_inv_sz,b_ovl)
        #print('T-1.B', np.shape(tb_sz))
        #with np.printoptions(precision=3, suppress=True):
        #    print(tb_sz)
        #
        # C_MO coefficients
        lsym=sim+1
        nol=norb(lsym)
        nbl=nbasis(lsym)
        # D = (T-1.B).C
        q=catchcmo(lsym)
        # cmoa = cmo1 <N-1|
        cmosza=cmoa[comtaboff[sim]:comtaboff[sim]+cmotab[sim]] 
        cmosza=np.reshape(cmosza,q[1],order='F')
        #print('CMO1',np.shape(cmosza))        
        #with np.printoptions(precision=4, suppress=True):
        #    print(cmosza)
        orb_sza = np.einsum('ik,kr->ir', tb_sz, cmosza) # D matrix for CMO1
        d_sza_sym.append(orb_sza.tolist())
        # cmob = cmo2 |N>
        cmoszb=cmob[comtaboff[sim]:comtaboff[sim]+cmotab[sim]]  
        cmoszb=np.reshape(cmoszb,q[1],order='F')
        #print('CMO2',np.shape(cmoszb))
        #with np.printoptions(precision=4, suppress=True):
        #    print(cmoszb)
        #print(' ----- ' )
        orb_szb = np.einsum('ik,kr->ir', tb_sz, cmoszb) # D matrix for CMO2
        d_szb_sym.append(orb_szb.tolist())
        
        #print('Orbital SZ1', np.shape(orb_sza))
        #with np.printoptions(precision=4, suppress=True):
        #    print(orb_sza)
         
        #print('Orbital SZ2', np.shape(orb_szb))
        #with np.printoptions(precision=4, suppress=True):
        #    print(orb_szb)
        #print(' ----- ' )

        #print('Overlap test original MOs')
        s_int_test=ao_overlap[comtbasoff2[sim]:comtbasoff2[sim]+(nbl**2)]
        s_int_test=np.reshape(s_int_test,(nbl,nbl))
        #print('CMO1*t.S.CMO2',np.shape(cmosza.T),np.shape(s_int_test),np.shape(cmoszb) )
        ffg=np.dot(cmosza.T,s_int_test )
        ffh=np.dot(ffg,cmoszb)
        #with np.printoptions(precision=4, suppress=True):
        #    print(ffh)
  
        #print('Overlap test projected MOs')        
        ffr=np.dot(orb_sza.T,sz_reshape )
        ffe=np.dot(ffr,orb_sza)
        #print(' ----- ' )
        #print('Overlap test: < || Psi_v - Psi^sz_v || > :',np.shape(orb_szb.T), np.shape(b_ovl) , np.shape(cmoszb))
        ffw=np.dot(orb_szb.T, b_ovl )
        ffq=np.dot(ffw,cmoszb)
        minimum0=np.add(ffh,ffe)
        minimum=np.add(minimum0,-2*ffq)
        #with np.printoptions(precision=4, suppress=True):
        #    print(minimum)
        #print(' ----- ' )
    temp2_sza = [x for x in d_sza_sym if x != []] # remove null elements from D for CMO1
    temp_sza = [val for sublist in temp2_sza for val in sublist] #Flatten the list 
    d_sza = [val for sublist in temp_sza for val in sublist] #Flatten the list 
    
    temp2_szb = [x for x in d_szb_sym if x != []] # remove null elements from D for CMO2
    temp_szb = [val for sublist in temp2_szb for val in sublist] #Flatten the list 
    d_szb = [val for sublist in temp_szb for val in sublist] #Flatten the list

#==================================
if auger_ri_true:
# Reading and working TUVX and RT2M on active orbital basis
    tuvx_scr=[]
    rt2m_scr=[]
    r=open('TUVX.dat',"r")
    lines=r.readlines()
    for i in range(nasht**4):
        scr=float(lines[i].split()[1])
        tuvx_scr.append(scr)
    r.close()

    t=open('rt2m.dat',"r")
    lines=t.readlines()
    for i in range(nasht**3):
        scr2=float(lines[i].split()[1])
        rt2m_scr.append(scr2)
    t.close()

    # this is not being used. 'active_tuvx' is not used
    # BRN. I use active_tuvx_22 instead.
    tuvx=np.reshape(tuvx_scr,(nasht,nasht,nasht,nasht))
    rt2m=np.reshape(rt2m_scr,(nasht,nasht,nasht))
    active_tuvx=np.einsum('ijkl,jkl->i ',tuvx,rt2m)
    # Frob Norm of the matrix r2TM
    norm_fr=0.0
    for i in range(nasht**3):
        norm_fr=norm_fr + rt2m_scr[i]**2
    print('Frobenius norm of r2TM',np.sqrt(norm_fr))
    #
    #print('------')
    # new R2TDM.TUVX proc.
    active_tuvx_22=list()
    for i in range(nasht):
        coef_tuvx=0.0
        ix=i+1
        for j in range(nasht):
            jx=j+1
            ij = ix + nasht*(jx-1)
            for k in range(nasht):
                kx=k+1
                for l in range(nasht):
                    lx=l+1
                    kl=kx+nasht*(lx-1)
                    lk=lx+nasht*(kx-1)
                    ijkl= int((ij-1)*nasht**2 + kl)
                    ijlk= int((ij-1)*nasht**2 + lk)
                    jkl=int(jx+nasht*(nasht*(kx-1)+lx-1))
                    #print(int(i+1),int(j+1),int(k+1),int(l+1),tuvx_scr[int(ijkl-1)],rt2m_scr[int(jkl-1)])
                    coef_tuvx=coef_tuvx+((tuvx_scr[int(ijkl-1)]-tuvx_scr[int(ijlk-1)])*rt2m_scr[int(jkl-1)])     
        active_tuvx_22.append(coef_tuvx)
    print('------')
    print('tuvx',active_tuvx_22)
#    print(active_tuvx)
    # add contribution from inactive orbitals. They are 0.
    tuvx_ab_scr=list()
    for js in range(symmetry):
        isym=js+1
        if nmo[js] > 0 :
            if nmo[js]-nash[js] > 0 : #have inactive orbitals. add 0s.
                tuvx_ab_scr.append(list(0.0 for i in range(nmo[js]-nash[js])))
            tuvx_ab_scr.append(active_tuvx_22[comtnashoff[js]:comtnashoff[js]+nash[js]])
    tuvx_ab=[val for sublist in tuvx_ab_scr for val in sublist] #Flatten the list
    #print('tuvx AB',tuvx_ab)
    # Now trasform to AO basis with CMO1 (cationic MO set).
    tuvx_ao=mo2ao(tuvx_ab,cmoa)
    # Print tuvx contracted with rt2m in AO basis as molden file
    #open a output file and print it there.
    w = open('tuvxrt2m.output', 'w')
    sys.stdout = w
    print('# TUVX contracted with RT2M in AO basis as molden file')
    tomolden(tuvx_ao)
    w.close()
#==================================

# store block symmetry for regular (direct) Dyson
direct_dys=list(0 for i in range(symmetry))
direct_dys_ao=list(0 for i in range(symmetry))

for s in range(symmetry):
    isym=s+1
    direct_dys[s]=dyson[comtcmoff[s]:comtcmoff[s]+norb(isym)]

#----------------------------
#open a output file and print all there.
g = open('rtdmzz.output', 'w')
sys.stdout = g
#----------------------------
# Computing direct Dyson orbital AO and norm**2
norm_ddys_tot=0.0
for l in range(symmetry):
    lsym=l+1
    nol=norb(lsym)
    nbl=nbasis(lsym)
    # direct Dyson AO transformation
    q=catchcmo(lsym)
    cmo2=cmob[comtaboff[l]:comtaboff[l]+cmotab[l]]
    cmo2=np.reshape(cmo2,q[1],order='F')
    ddys=direct_dys[l]
    ddys_ao = np.einsum('ij,j->i', cmo2, ddys)
#   store symmetry block in direct_dys_ao
    direct_dys_ao[l]=ddys_ao
    s_int=ao_overlap[comtbasoff2[l]:comtbasoff2[l]+(nbl**2)]
    s_int=np.reshape(s_int,(nbl,nbl))
    norm_ddys=  np.einsum('ij,j->i',s_int,ddys_ao)
    norm_ddys=  np.einsum('j,j->',norm_ddys,ddys_ao)
    # square norm of direct Dyson term
    norm_ddys_tot=norm_ddys_tot+norm_ddys
    
#------------------------------------------------
# Here I print the direct Dyson Orb. (with symmetrized basis standard)
# [val for sublist in list_of_lists for val in sublist] is a simple one-line way to flat a list_of_lists
flatten_dys=[val for sublist in direct_dys_ao for val in sublist]
direct_dys_ao=flatten_dys
if print_direct_dys:
    print('# One particle direct Dyson orbital with symmetrized basis (non-Molden file)')
    for i in range(nbasft):
        print( '{:^3} {:7} {:19.16f}'.format(i+1,basis_id_list[i],direct_dys_ao[i]) )
    print('# Sqr Norm direct dyson',norm_ddys_tot)
print(' ')
#------------------------------------------------
print('# DIRECT DYSON ORBITAL AS MOLDEN FILE')
# tomolden() is the function called to print an orbital
tomolden(direct_dys_ao)
print(' ')
#-------------------

countorb=0
countbasis=0
countbasis_sz=0
startorb=0
totfrob=0.0
tdmzz_symm=0.0
norm_cdys=0.0
norm_cdys_tot=0.0

Auger_width=0.0
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%      If Fink projection          %
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if fink_projection:
    # ELM (L,M) contains 9 elements: { [0,0]; [1,-1]; [1,0]; [1,1]; [2,-2]; [2,-1]; [2,0]; [2,1]; [2,2] }
    ELM=list(0 for ij in range(9))
    print('#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('#%      If Fink projection          %')
    print('#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('#The MOs are being projected into the atomic basis functions')
    [print(basis_id_list[ei]) for ei in shell_basis]   
    print('# One Center Approximation')
    print('# The edge is on the basis function :', OCA_center )
    print('#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    print('#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
#
# cdys_r stores the coefficients for the conj. Dys orb in MO basis at each dip. op.
cdys_x=list(0 for ij in range(symmetry))
cdys_y=list(0 for ij in range(symmetry))
cdys_z=list(0 for ij in range(symmetry))
for ib in range(symmetry):
    no=norb(ib+1)
    cdys_x[ib]=list(0 for ib in range(no))
    cdys_y[ib]=list(0 for ib in range(no))
    cdys_z[ib]=list(0 for ib in range(no))
for i in range(symmetry):
    isym=i+1
    noi=norb(isym)
    nbi=nbasis(isym)
    if fink_projection:
        nbi_sz=nbasis_sz(isym)
    for j in range(symmetry):
        jsym=j+1
        noj=norb(jsym)
        nbj=nbasis(jsym)
        if fink_projection:
            nbj_sz=nbasis_sz(jsym)
        for l in range(symmetry):
            lsym=l+1
            nol=norb(lsym)
            nbl=nbasis(lsym)
            if fink_projection:
                nbl_sz=nbasis_sz(lsym)
            if noi !=0 and noj !=0 and nol !=0:
                if mul((mul(isym,jsym)),lsym)==totalSymmetry:
                    del(tdmzz_symm)
            # Compute conjugated Dyson.
            # Start by getting the Dipole matrix in MO basis
                    rmo_logic=False
                    if conjdys:
                        rmo=list()
                        for op in range(3):
                            op=op+1
                            syop=symop(op)
                            if mul(isym,jsym)==syop:
                                rmo_logic=True
                                dip=ao_dip[op-1]
                                dipao=dip[comtbasoff[i]:comtbasoff[i]+nbasf[i],comtbasoff[j]:comtbasoff[j]+nbasf[j]]
                                w=catchcmo(isym)
                                cmo1=cmoa[comtaboff[i]:comtaboff[i]+cmotab[i]]
                                cmo1=np.reshape(cmo1,w[1],order='F')
                                q=catchcmo(jsym)
                                cmo2=cmob[comtaboff[j]:comtaboff[j]+cmotab[j]]
                                cmo2=np.reshape(cmo2,q[1],order='F')
                                print("# 1-e Conj. Dys. Orb. symmetry (isym,jsym),(syop):",'(',isym,',',jsym,')','(',syop,')')
                                #Block symmetry product isym,jsym,lsym
                                scr11 = np.einsum('ia,ij->aj', cmo1,dipao)
                                # rmo is the dipole matrix in mo basis
                                rmo=np.einsum('aj,jb->ab', scr11 ,cmo2)
                                #print('# shape of rmo:',np.shape(rmo))
                                opersym=syop
                    norbtotal=noi*noj*nol
                    nbastotal=nbi*nbj*nbl
                    if fink_projection:
                        nbastotal_sz=nbi_sz*nbj_sz*nbl_sz
                        countbasis_sz=countbasis_sz + nbastotal_sz
                    countorb=countorb+norbtotal
                    countbasis=countbasis+nbastotal
                    #take symmetry tdmab and reshape
                    rtdmab=tdmab[startorb:countorb]
                    rtdmab=np.reshape(rtdmab,(noi,noj,nol))
                    # compute conjugated Dyson orbital coeff.
                    if rmo_logic:
                        # make in MO basis
                        #print("# symmetry:",isym,jsym,lsym)
                        #print('# shape of rmo,rtdmab:',np.shape(rmo),np.shape(rtdmab))
                        cdys = np.einsum('ij,ijl->l', rmo, rtdmab)
                        # make in AO basis
                        q=catchcmo(lsym)
                        cmo2=cmob[comtaboff[l]:comtaboff[l]+cmotab[l]]
                        cmo2=np.reshape(cmo2,q[1],order='F')
                        print("# symmetry Conj Dyson Orb:",lsym,', symm Op:',symoprep(opersym) )
                        cdys_ao = np.einsum('ij,j->i', cmo2, cdys)
                        if symoprep(opersym)=='x':
                            # accumulate the X components in mocdys_x
                            cdys_x[lsym-1]=cdys_x[lsym-1]+cdys
                        elif symoprep(opersym)=='y':
                            # accumulate the X components in mocdys_y
                            cdys_y[lsym-1]=cdys_y[lsym-1]+cdys
                        elif symoprep(opersym)=='z':
                            # accumulate the X components in mocdys_z
                            cdys_z[lsym-1]=cdys_z[lsym-1]+cdys
                    #catch symmetry cmoa and cmob and reshape
                    #catchcmo(isym)[0] is the product #Orb*#Basis in that symm
                    #catchcmo(isym)[1] is the tuple(#basis,#orb)
                    if not fink_projection:
                        x=catchcmo(isym)
                        cmo1=cmoa[comtaboff[i]:comtaboff[i]+cmotab[i]]
                        cmo1=np.reshape(cmo1,x[1],order='F')
                        #
                        y=catchcmo(jsym)
                        cmo21=cmob[comtaboff[j]:comtaboff[j]+cmotab[j]]
                        cmo21=np.reshape(cmo21,y[1],order='F')
                        #with np.printoptions(precision=4, suppress=True):
                        #    print(cmo21)
                        z=catchcmo(lsym)
                        cmo22=cmob[comtaboff[l]:comtaboff[l]+cmotab[l]]
                        cmo22=np.reshape(cmo22,z[1],order='F')
                        #Block symmetry product isym,jsym,lsym
                        scr1=np.einsum('ai,ijl->ajl', cmo1,rtdmab)
                        scr2=np.einsum('ajl,jb->abl',scr1,np.transpose(cmo21))
                        tdmzz_symm=np.einsum('abl,lc->abc',scr2,np.transpose(cmo22))
                    else:
                        x=catchcmo_sz(isym)
                        cmo1=d_sza[norbsztaboff[i]:norbsztaboff[i]+nszorb[i]]
                        cmo1=np.reshape(cmo1,x[1],order='C')
                        #
                        y=catchcmo_sz(jsym)
                        cmo21=d_szb[norbsztaboff[j]:norbsztaboff[j]+nszorb[j]]
                        cmo21=np.reshape(cmo21,y[1],order='C')
                        #with np.printoptions(precision=4, suppress=True):
                        #    print(cmo21)
                        z=catchcmo_sz(lsym)
                        cmo22=d_szb[norbsztaboff[l]:norbsztaboff[l]+nszorb[l]]
                        cmo22=np.reshape(cmo22,z[1],order='C')
                        #Block symmetry product isym,jsym,lsym
                        scr1=np.einsum('ai,ijl->ajl', cmo1,rtdmab)
                        scr2=np.einsum('ajl,jb->abl',scr1,np.transpose(cmo21))
                        tdmzz_symm=np.einsum('abl,lc->abc',scr2,np.transpose(cmo22))
                    # ---------    
                    #print tdmzz_symm in 1-D
                    if not fink_projection:
                        print("# symmetry:",isym,jsym,lsym)
                        print('# shape of tdmzz:',np.shape(tdmzz_symm),', num elements:',nbastotal)
                        for izz in range(np.shape(tdmzz_symm)[0]):
                            totfrob=totfrob+(LA.norm(tdmzz_symm[izz],'fro')**2)
                            print('# Frobenius norm of TDMZZ on orb.',izz+1,':',LA.norm(tdmzz_symm[izz],'fro'))
                            for jzz in range(np.shape(tdmzz_symm)[1]):
                                for lzz in range(np.shape(tdmzz_symm)[2]):
                                    print(izz+1,jzz+1,lzz+1, "%.12E" % tdmzz_symm[izz][jzz][lzz])
                    #
                    if fink_projection: # I am doing OCA projection
                        fizz=range(np.shape(tdmzz_symm)[0])
                        fjzz=range(np.shape(tdmzz_symm)[1])
                        flzz=range(np.shape(tdmzz_symm)[2])

                        #print('# shape of tdmzz:',np.shape(tdmzz_symm),', num elements:',nbastotal)
                        for izz in fizz:
                            #print('# Frobenius norm of TDMZZ on orb.',izz+1,':',LA.norm(tdmzz_symm[izz],'fro'))
                            totfrob=totfrob+(LA.norm(tdmzz_symm[izz],'fro'))
                            # first element must be the core
                            if atombasis_list[izz+nszoff[i]] == OCA_center : 
                                print("# symmetry:",isym,jsym,lsym)
                                #for jzz in fjzz:
                                    #for lzz in flzz:
                                #Accumulate the integral ELM
                                #print('Elm_ij',tdmzz_symm[izz][jzz][lzz])
                                for ll in range(3): # L=0,1,2 
                                    for mm in range(-ll,ll+1): # M = -L,L
                                        
                                        for jzz in fjzz:
                                            for lzz in flzz:
                                                if atombasis_list[jzz+nszoff[j]][:2]==OCA_center[:2] and atombasis_list[lzz+nszoff[l]][:2]==OCA_center[:2] :
                                                    gc=atombasis_list[izz+nszoff[i]]
                                                    gi=atombasis_list[jzz+nszoff[j]][2:].replace(" ","")
                                                    gj=atombasis_list[lzz+nszoff[l]][2:].replace(" ","")
                                                    if ll==0 and mm==0:
                                                        ELM[0] = ELM[0] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==1 and mm==-1:
                                                        ELM[1] = ELM[1] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==1 and mm==0:
                                                        ELM[2] = ELM[2] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==1 and mm==1:
                                                        ELM[3] = ELM[3] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==2 and mm==-2:
                                                        ELM[4] = ELM[4] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==2 and mm==-1:
                                                        ELM[5] = ELM[5] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==2 and mm==0:
                                                        ELM[6] = ELM[6] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==2 and mm==1:
                                                        ELM[7] = ELM[7] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if ll==2 and mm==2:
                                                        ELM[8] = ELM[8] + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    #ELM = ELM + tdmzz_symm[izz][jzz][lzz]*elmij(gc,gi,gj,ll,mm)
                                                    if abs(elmij(gc,gi,gj,ll,mm) )>10e-9:
                                                        print(gc,',',atombasis_list[jzz+nszoff[j]],',',atombasis_list[lzz+nszoff[l]],',',\
                                                             "%.12E" % tdmzz_symm[izz][jzz][lzz],';','elm(',ll,mm,')',':',elmij(gc,gi,gj,ll,mm) )
                                            #       #print('I,J,L,M,Gml:',jzz+nszoffA[j],lzz+nszoffA[l] ,ll,mm, elmij(piedec,piedei,piedej,ll,mm),ELM )
                    startorb=countorb

if not fink_projection:
    print('#total number of #orb and #basis:',countorb,countbasis)
if fink_projection:
    ELM2=[elimm**2 for elimm in ELM]
    print('# Auger One center approx = ', 4.0*3.14159265359*(sum(ELM2)) )

print('# Total Frobenius norm of TDMZZ',totfrob)
print('# The End ')

g.close()

#------------------------------------------------
if print_direct_dys:
    if  norm_ddys_tot > 10**-14:
        dys_dir_print = open('direct_dyson.output', 'a')
        sys.stdout = dys_dir_print
        print('#1-p DYSON ORBITAL AS MOLDEN FILE:',norm_ddys_tot)
    # tomolden() is the function called to print an orbital
        tomolden(direct_dys_ao)
        dys_dir_print.close()
#------------------------------------------------

if conjdys==True and norm_ddys_tot > 10**-14: 
    if norb(mul(totalSymmetry,symop(1)))!=0:
        cx = open('conjx.output', 'a')
        sys.stdout = cx
        #print('#Molden conj. Dyson orb. component x')
        conjugate_dys_ao=list(0 for i in range(symmetry))
        norm_conjdys_tot=0.0
        for l in range(symmetry):
            lsym=l+1
            nol=norb(lsym)
            nbl=nbasis(lsym)
            # conj Dyson AO transformation
            q=catchcmo(lsym)
            cmo2=cmob[comtaboff[l]:comtaboff[l]+cmotab[l]]
            cmo2=np.reshape(cmo2,q[1],order='F')
            conjdys_r=cdys_x[l]
            conjdys_ao = np.einsum('ij,j->i', cmo2, conjdys_r)
#   store symmetry block in conjugate_dys_ao[l]
            conjugate_dys_ao[l]=conjdys_ao
            s_int=ao_overlap[comtbasoff2[l]:comtbasoff2[l]+(nbl**2)]
            s_int=np.reshape(s_int,(nbl,nbl))
            norm_cdys=  np.einsum('ij,j->i',s_int,conjdys_ao)
            norm_cdys=  np.einsum('j,j->',norm_cdys,conjdys_ao)
            # square norm of direct Dyson term
            norm_conjdys_tot=norm_conjdys_tot+norm_cdys
#-------------------
        flatten_cdys=[val for sublist in conjugate_dys_ao for val in sublist]
        conjugate_dys_ao=flatten_cdys
        print('# Conj. Dyson symm:',mul(totalSymmetry,symop(1)),'=',totalSymmetry,'.X component; norm:',norm_conjdys_tot)
        tomolden(conjugate_dys_ao)
        cx.close()
#-----------------------------------------------
#-----------------------------------------------
    if norb(mul(totalSymmetry,symop(2)))!=0:
        cy = open('conjy.output', 'a')
        sys.stdout = cy
        #print('#Molden conj. Dyson orb. component y')
        conjugate_dys_ao=list(0 for i in range(symmetry))
        norm_conjdys_tot=0.0
        for l in range(symmetry):
            lsym=l+1
            nol=norb(lsym)
            nbl=nbasis(lsym)
# conj Dyson AO transformation
            q=catchcmo(lsym)
            cmo2=cmob[comtaboff[l]:comtaboff[l]+cmotab[l]]
            cmo2=np.reshape(cmo2,q[1],order='F')
            conjdys_r=cdys_y[l]
            conjdys_ao = np.einsum('ij,j->i', cmo2, conjdys_r)
#   store symmetry block in conjugate_dys_ao[l]
            conjugate_dys_ao[l]=conjdys_ao
            s_int=ao_overlap[comtbasoff2[l]:comtbasoff2[l]+(nbl**2)]
            s_int=np.reshape(s_int,(nbl,nbl))
            norm_cdys=  np.einsum('ij,j->i',s_int,conjdys_ao)
            norm_cdys=  np.einsum('j,j->',norm_cdys,conjdys_ao)
# square norm of direct Dyson term
            norm_conjdys_tot=norm_conjdys_tot+norm_cdys
#----------------
        flatten_cdys=[val for sublist in conjugate_dys_ao for val in sublist]
        conjugate_dys_ao=flatten_cdys
        print('# Conj. Dyson symm:',mul(totalSymmetry,symop(2)),'=',totalSymmetry,'.Y component; norm:',norm_conjdys_tot)
        tomolden(conjugate_dys_ao)
        cy.close()
#------------------------------------------------
#------------------------------------------------
    if norb(mul(totalSymmetry,symop(3)))!=0:
        cz = open('conjz.output', 'a')
        sys.stdout = cz
#print('#Molden conj. Dyson orb. component z')
        conjugate_dys_ao=list(0 for i in range(symmetry))
        norm_conjdys_tot=0.0
        for l in range(symmetry):
            lsym=l+1
            nol=norb(lsym)
            nbl=nbasis(lsym)
# conj Dyson AO transformation
            q=catchcmo(lsym)
            cmo2=cmob[comtaboff[l]:comtaboff[l]+cmotab[l]]
            cmo2=np.reshape(cmo2,q[1],order='F')
            conjdys_r=cdys_z[l]
            conjdys_ao = np.einsum('ij,j->i', cmo2, conjdys_r)
#   store symmetry block in conjugate_dys_ao[l]
            conjugate_dys_ao[l]=conjdys_ao
            s_int=ao_overlap[comtbasoff2[l]:comtbasoff2[l]+(nbl**2)]
            s_int=np.reshape(s_int,(nbl,nbl))
            norm_cdys=  np.einsum('ij,j->i',s_int,conjdys_ao)
            norm_cdys=  np.einsum('j,j->',norm_cdys,conjdys_ao)
# square norm of direct Dyson term
            norm_conjdys_tot=norm_conjdys_tot+norm_cdys
#--------------
        flatten_cdys=[val for sublist in conjugate_dys_ao for val in sublist]
        conjugate_dys_ao=flatten_cdys
        print('# Conj. Dyson symm:',mul(totalSymmetry,symop(3)),'=',totalSymmetry,'.Z component; norm:',norm_conjdys_tot)
        tomolden(conjugate_dys_ao)
        cz.close()

